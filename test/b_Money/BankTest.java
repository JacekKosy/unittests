package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BankTest {
	Currency SEK, DKK;
	Bank SweBank, Nordea, DanskeBank;
	
	@Before
	public void setUp() throws Exception {
		DKK = new Currency("DKK", 0.20);
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		Nordea = new Bank("Nordea", SEK);
		DanskeBank = new Bank("DanskeBank", DKK);
		SweBank.openAccount("Ulrika");
		SweBank.openAccount("Bob");
		Nordea.openAccount("Bob");
		DanskeBank.openAccount("Gertrud");
	}

	@Test
	public void testGetName() {
		//test verifies whether name getter returns correct name, with case sensitivity
		assertEquals("Nordea", Nordea.getName());
		assertEquals("SweBank", SweBank.getName());
		assertNotEquals("danskeBank", DanskeBank.getName());
	}

	@Test
	public void testGetCurrency() {
		//test verifies whether the getter method returns correct currency
		assertEquals(SEK, SweBank.getCurrency());
		assertEquals(DKK, DanskeBank.getCurrency());
	}

	@Test
	public void testOpenAccount() throws AccountExistsException {
		//usual flow - account not exists
		var accountId = "1ED23";
		Nordea.openAccount(accountId);
		assertTrue(Nordea.getAccountList().containsKey(accountId));
		//alternate flow - account exists
		try {
			var accountIdB = "12P09";
			SweBank.openAccount(accountIdB);
			SweBank.openAccount(accountIdB);
			//exception is not thrown
			fail("account is created for the second time");
		} catch (AccountExistsException e) {
			//exception is thrown - test passes
			System.out.println("Account already exists");
		}
	}

	@Test
	public void testDeposit() throws AccountDoesNotExistException {
		//usual flow - money is deposited
		// add method is tested elsewhere
		Money deposit = new Money(10000, SEK);
		SweBank.deposit("Ulrika", deposit);
		assertTrue(deposit.equals(SweBank.getAccountList().get("Ulrika").getBalance()));
		try {
			//alternate flow - trying to deposit into non-existing account
			Nordea.deposit("123", deposit);
			fail("Money is deposited into non-existing account");
		} catch (AccountDoesNotExistException e) {
			//exception is thrown - test passes
			System.out.println("Account doesn't exist");
		}
	}

	@Test
	public void testWithdraw() throws AccountDoesNotExistException {
		//usual flow - money is withdrawn
		// add method is tested elsewhere
		Money withdrawal = new Money(10000, DKK);
		DanskeBank.withdraw("Gertrud", withdrawal);
		assertTrue(withdrawal.negate().equals(DanskeBank.getAccountList().get("Gertrud").getBalance()));
		try {
			//alternate flow - trying to withdraw from non-existing account
			Nordea.withdraw("123", withdrawal);
			fail("Money is withdrawn from non-existing account");
		} catch (AccountDoesNotExistException e) {
			//exception is thrown - test passes
			System.out.println("Account doesn't exist");
		}
	}
	
	@Test
	public void testGetBalance() throws AccountDoesNotExistException {
		//usual flow - balance is returned
		//deposit method is tested above
		Money expectedBalance = new Money(10000, SEK);
		Nordea.deposit("Bob", expectedBalance);
		assertEquals(expectedBalance.getAmount(), Nordea.getBalance("Bob"));
		//alternate flow - account doesn't exist
		try {
			//alternate flow - trying to withdraw from non-existing account
			Nordea.getBalance("123");
			fail("Checking balance of a non-existing account");
		} catch (AccountDoesNotExistException e) {
			//exception is thrown - test passes
			System.out.println("Account doesn't exist");
		}
	}
	
	@Test
	public void testTransfer() throws AccountDoesNotExistException {
		//flow one - two different banks
		Money amountToBeTransfered = new Money(10000, SEK);
		Nordea.deposit("Bob", amountToBeTransfered);
		Nordea.transfer("Bob", SweBank, "Ulrika", amountToBeTransfered);
		assertEquals(0, (int) Nordea.getBalance("Bob"));
		assertEquals(10000, (int) SweBank.getBalance("Ulrika"));
		//flow two - same bank
		SweBank.transfer("Ulrika", "Bob", amountToBeTransfered);
		assertEquals(0, (int) SweBank.getBalance("Ulrika"));
		assertEquals(10000, (int) SweBank.getBalance("Bob"));
		//flow three - transferring into non-existing account
		try {
			//alternate flow - trying to withdraw from non-existing account
			Nordea.transfer("Bob", Nordea, "John", amountToBeTransfered);
			fail("Transferring to a non-existing account");
		} catch (AccountDoesNotExistException e) {
			//exception is thrown - test passes
			System.out.println("Account doesn't exist");
		}
	}
	
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		fail("Write test case here");
	}
}
