package b_Money;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CurrencyTest {
	Currency SEK;
	Currency DKK;
	Currency EUR;
	
	@Before
	public void setUp() {
		/* Setup currencies with exchange rates */
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
	}

	@Test
	public void testGetName() {
		//Checking whether the getter method for name returns correct result
		assertEquals("SEK", SEK.getName());
		assertEquals("DKK", DKK.getName());
		assertEquals("EUR", EUR.getName());
	}
	@Test
	public void testGetRate() {
		//Checking whether the getter method for rate returns correct result
		assertEquals(Double.valueOf(0.15), SEK.getRate());
		assertEquals(Double.valueOf(0.20), DKK.getRate());
		assertEquals(Double.valueOf(1.5), EUR.getRate());
	}
	
	@Test
	public void testSetRate() {
		//Checking whether the setter method changes the value of rate
		assertEquals(Double.valueOf(1.5), EUR.getRate());
		EUR.setRate(1.4);
		assertEquals(Double.valueOf(1.4), EUR.getRate());
	}
	
	@Test
	public void testGlobalValue() {
		//Checking whether the global value method returns correct result for each currency example
		Integer hundredSEKGlobalValue = 1500;
		assertEquals(hundredSEKGlobalValue, SEK.universalValue(10000));
		Integer sixtyDKKGlobalValue = 1240;
		assertEquals(sixtyDKKGlobalValue, DKK.universalValue(6200));
		Integer thirtyEURGlobalValue = 4500;
		assertEquals(thirtyEURGlobalValue, EUR.universalValue(3000));
	}
	
	@Test
	public void testValueInThisCurrency() {
		//checking whether currencies convert correctly
		Integer hundredEURinSEK = 100000;
		assertEquals(hundredEURinSEK, SEK.valueInThisCurrency(10000, EUR));
		Integer hundredEURinDKK = 75000;
		assertEquals(hundredEURinDKK, DKK.valueInThisCurrency(10000, EUR));
	}

}
