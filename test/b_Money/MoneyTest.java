package b_Money;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoneyTest {
	Currency SEK, DKK, NOK, EUR;
	Money SEK100, EUR10, SEK200, EUR20, SEK0, EUR0, SEKn100;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
		SEK100 = new Money(10000, SEK);
		EUR10 = new Money(1000, EUR);
		SEK200 = new Money(20000, SEK);
		EUR20 = new Money(2000, EUR);
		SEK0 = new Money(0, SEK);
		EUR0 = new Money(0, EUR);
		SEKn100 = new Money(-10000, SEK);
	}

	@Test
	public void testGetAmount() {
		//test cases for the getAmount method - Swedish krona
		assertEquals(Integer.valueOf(-10000), SEKn100.getAmount());
		assertEquals(Integer.valueOf(0), SEK0.getAmount());
		assertEquals(Integer.valueOf(10000), SEK100.getAmount());
		assertEquals(Integer.valueOf(20000), SEK200.getAmount());
		//test cases for the getAmount method - euro
		assertEquals(Integer.valueOf(0), EUR0.getAmount());
		assertEquals(Integer.valueOf(1000), EUR10.getAmount());
		assertEquals(Integer.valueOf(2000), EUR20.getAmount());
	}

	@Test
	public void testGetCurrency() {
		//checks whether currency getter works correctly for both examples
		Currency swedishKrona = SEK100.getCurrency();
		assertEquals(SEK.getName(), swedishKrona.getName());
		assertEquals(SEK.getRate(), swedishKrona.getRate());
		Currency euro = EUR0.getCurrency();
		assertEquals(EUR.getName(), euro.getName());
		assertEquals(EUR.getRate(), euro.getRate());
	}

	@Test
	public void testToString() {
		String tenEuroString = EUR10.toString();
		assertEquals("10.00 EUR", tenEuroString);
		String hundredKronaString = SEK100.toString();
		assertEquals("100.00 SEK", hundredKronaString);
	}

	@Test
	public void testGlobalValue() {
		//checking whether universalValue method calculates result correctly with 3 examples
		Integer expectedForSEK100 = 1500;
		assertEquals(expectedForSEK100, SEK100.universalValue());
		Integer expectedForEUR20 = 3000;
		assertEquals(expectedForEUR20, EUR20.universalValue());
		Integer expectedForSEKn100 = -1500;
		assertEquals(expectedForSEKn100, SEKn100.universalValue());
	}

	@Test
	public void testEqualsMoney() {
		//checking whether equals method returns correct result with three different examples
		assertTrue(SEK0.equals(EUR0));
		assertFalse(SEK200.equals(EUR10));
		assertTrue(EUR10.equals(SEK100));
	}

	@Test
	public void testAdd() {
		//checking whether add method returns correct result for three cases - with and without currency conversion
		var expectedA = new Money(20000, SEK);
		var actualA = SEK100.add(EUR10);
		assertTrue(expectedA.equals(actualA));
		var expectedB = new Money(1000, EUR);
		var actualB = EUR20.add(SEKn100);
		assertTrue(expectedB.equals(actualB));
		var expectedC = new Money(3000, EUR);
		var actualC = EUR10.add(EUR20);
		assertTrue(expectedC.equals(actualC));
	}

	@Test
	public void testSub() {
		//checking whether subtract method returns correct result for three cases - with and without currency conversion
		var expectedA = new Money(500, EUR);
		var actualA = EUR20.sub(new Money(15000, SEK));
		assertTrue(expectedA.equals(actualA));
		var expectedB = new Money(-20000, SEK);
		var actualB = SEK100.sub(EUR20).sub(EUR10);
		assertTrue(expectedB.equals(actualB));
		var expectedC = new Money(30000, SEK);
		var actualC = SEK200.sub(SEKn100);
		assertTrue(expectedC.equals(actualC));
	}

	@Test
	public void testIsZero() {
		//checking whether isZero method returns correct value - both for true and false
		assertTrue(EUR0.isZero());
		assertTrue(SEK100.add(SEKn100).isZero());
	}

	@Test
	public void testNegate() {
		//checking whether negate method works correctly - usual case and corner case for zero
		assertTrue(SEK100.negate().equals(SEKn100));
		assertTrue(EUR0.negate().equals(EUR0));
	}

	@Test
	public void testCompareTo() {
		//checking whether compareTo method returns correct value - for all three cases
		assertEquals(EUR20.compareTo(SEK100), 1);
		assertEquals(EUR10.compareTo(SEK100), 0);
		assertEquals(SEKn100.compareTo(EUR0), -1);
	}
}
