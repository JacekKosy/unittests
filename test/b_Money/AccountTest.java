package b_Money;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AccountTest {
	Currency SEK, DKK;
	Bank Nordea;
	Bank DanskeBank;
	Bank SweBank;
	Account testAccount;
	
	@Before
	public void setUp() throws Exception {
		SEK = new Currency("SEK", 0.15);
		SweBank = new Bank("SweBank", SEK);
		SweBank.openAccount("Alice");
		testAccount = new Account(SEK);
		testAccount.deposit(new Money(10000000, SEK));

		SweBank.deposit("Alice", new Money(1000000, SEK));
	}
	
	@Test
	public void testAddRemoveTimedPayment() {
		fail("Write test case here");
	}
	
	@Test
	public void testTimedPayment() throws AccountDoesNotExistException {
		fail("Write test case here");
	}

	@Test
	public void testAddWithdraw() {
		var amount = new Money(10000000, SEK);
		testAccount.withdraw(amount);
		assertTrue(testAccount.getBalance().isZero());
		testAccount.deposit(amount);
		assertTrue(new Money(10000000, SEK).equals(testAccount.getBalance()));
	}
	
	@Test
	public void testGetBalance() {
		//checking whether getBalance returns correct amount
		assertTrue(new Money(10000000, SEK).equals(testAccount.getBalance()));
	}
}
